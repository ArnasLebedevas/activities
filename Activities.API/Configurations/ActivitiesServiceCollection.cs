﻿using Activities.API.Configurations.Activities;
using Activities.API.Infrastructure;

namespace Activities.API.Configurations
{
    public class ActivitiesServiceCollection : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services.Add(new ActivityConfiguration());
        }
    }
}
