﻿using Activities.API.Infrastructure;
using Activities.CommandHandlers.Activities.Delete;
using Activities.CommandHandlers.Activities.Save;
using Activities.Data.Core.Activities;
using Activities.Data.Core.Data;
using Activities.Domain.Activities;
using Activities.Models.Activities;
using Activities.Models.Base.Commands;

namespace Activities.API.Configurations.Activities
{
    public class ActivityConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services
                .AddCommandHandler<ActivityModel, ActivitySaveHandler>()
                .AddCommandHandler<DeleteCommand<ActivityModel>, ActivityDeleteHandler>()
                .AddRepository<Activity, DataRepository<Activity>>()
                .AddMapper<ActivityModel, Activity, ActivityMapper>();

            services.AddTransient<IActivityRepository, ActivityRepository>();
        }
    }
}
