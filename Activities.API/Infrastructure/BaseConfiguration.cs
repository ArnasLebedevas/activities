﻿using Activities.CommandHandlers.Base;
using Activities.Data.Core.Context.Infrastructure;
using Activities.Models.Base;
using Activities.Models.Base.Commands;
using Activities.Models.Base.Infrastructure;

namespace Activities.API.Infrastructure
{
    public class BaseConfiguration : IServiceConfiguration
    {
        public void Configure(IServiceCollection services)
        {
            services.AddTransient<IBus, Bus>();
            services.AddTransient<IDependencyResolver, DependencyResolver>();
            services.AddTransient<IValidator, Validator>();
            services.AddTransient<IWriteContext, WriteContextAdapter>();
        }
    }
}
