﻿using Activities.Domain;
using Activities.Models.Base;
using Activities.Models.Base.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Activities.API.Infrastructure
{
    public class ServiceCollectionAdapter : IServiceCollection
    {
        private readonly Microsoft.Extensions.DependencyInjection.IServiceCollection serviceCollection;

        public ServiceCollectionAdapter(Microsoft.Extensions.DependencyInjection.IServiceCollection serviceCollection)
        {
            this.serviceCollection = serviceCollection;
        }

        public void AddTransient<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService
        {
            serviceCollection.AddTransient<TService, TImplementation>();
        }

        IServiceCollection IServiceCollection.AddCommandHandler<TCommand, THandler>()
        {
            AddTransient<ICommandHandler<TCommand>, THandler>();

            return this;
        }

        IServiceCollection IServiceCollection.AddMapper<TModel, TEntity, TMapper>()
        {
            AddTransient<IMapper<TModel, TEntity>, TMapper>();

            return this;
        }

        IServiceCollection IServiceCollection.AddRepository<TEntity, TRepository>()
        {
            AddTransient<IRepository<TEntity>, TRepository>();

            return this;
        }

        IServiceCollection IServiceCollection.AddRule<TCommand, TRule>()
        {
            AddTransient<IRule<TCommand>, TRule>();

            return this;
        }
    }
}
