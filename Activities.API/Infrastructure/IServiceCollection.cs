﻿using Activities.Domain;
using Activities.Models.Base;
using Activities.Models.Base.Commands;

namespace Activities.API.Infrastructure
{
    public interface IServiceCollection
    {
        void AddTransient<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService;

        IServiceCollection AddCommandHandler<TCommand, THandler>()
            where THandler : class, ICommandHandler<TCommand>;

        IServiceCollection AddRepository<TEntity, TRepository>()
            where TRepository : class, IRepository<TEntity>;

        IServiceCollection AddMapper<TModel, TEntity, TMapper>()
            where TMapper : class, IMapper<TModel, TEntity>;

        IServiceCollection AddRule<TCommand, TRule>()
            where TRule : class, IRule<TCommand>;
    }
}
