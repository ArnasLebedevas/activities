﻿using Activities.Models.Base;
using Activities.Models.Base.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Activities.API.Infrastructure
{
    public class Bus : IBus
    {
        private readonly IServiceProvider serviceProvider;
        private readonly ILogger<Bus> logger;
        private readonly IValidator validator;

        public Bus(IServiceProvider serviceProvider, ILogger<Bus> logger, IValidator validator)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
            this.validator = validator;
        }

        public Task<TResult> Query<TParam, TResult>(TParam param)
        {
            throw new SystemException();
        }

        public async Task<ICommandResult> Send<TCommand>(TCommand command)
        {
            ICommandResult validationResult = await Validate(command);

            if(validationResult != null && !validationResult.IsValid)
            {
                return validationResult;
            }

            return await Execute(command);
        }

        private Task<ICommandResult> Execute<TCommand>(TCommand command)
        {
            try
            {
                ICommandHandler<TCommand> handler = ResolveCommandHandler<TCommand>();

                if(handler == null)
                {
                    throw new Exception("Command handler for " + command.GetType().FullName + " not found.");
                }

                return handler.Handle(command);
            }
            catch(Exception ex)
            {
                logger.LogError(ex.Message);

                throw;
            }
        }

        private ICommandHandler<TCommand> ResolveCommandHandler<TCommand>()
        {
            return serviceProvider.GetService(typeof(ICommandHandler<TCommand>)) as ICommandHandler<TCommand>;
        }

        private Task<ICommandResult> Validate<TCommand>(TCommand command)
        {
            IEnumerable<IRule<TCommand>> rules = ResolveValidator<TCommand>();

            if(validator == null)
            {
                return Task.FromResult<ICommandResult>(null);
            }

            return validator.Validate(command, rules);
        }

        private IEnumerable<IRule<TCommand>> ResolveValidator<TCommand>()
        {
            return serviceProvider.GetServices<IRule<TCommand>>();
        }
    }
}
