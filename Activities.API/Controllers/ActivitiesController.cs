﻿using Activities.API.Controllers.Base.ApiControllers;
using Activities.Models.Activities;
using Activities.Models.Base;

namespace Activities.API.Controllers
{
    public class ActivitiesController : BaseApiController<ActivityModel>
    {
        public ActivitiesController(IBus bus) : base(bus)
        { }
    }
}
