﻿using Activities.Models.Base;
using Activities.Models.Base.Commands;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Activities.API.Controllers.Base.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController<TModel> : ControllerBase
    {
        protected IBus mediator;

        public BaseApiController(IBus mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult> Save(TModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var saveResult = await mediator.Send(model);

            if (!saveResult.IsValid)
            {
                return BadRequest(saveResult);
            }

            return Created("", model);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var command = new DeleteCommand<TModel>(id);

            var deleteResult = await mediator.Send(command);

            if (!deleteResult.IsValid)
            {
                return BadRequest(deleteResult);
            }

            return NoContent();
        }
    }
}
