﻿namespace Activities.Domain.Activities
{
    public interface IActivityRepository : IRepository<Activity>
    { }
}
