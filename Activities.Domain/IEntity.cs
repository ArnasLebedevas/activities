﻿namespace Activities.Domain
{
    public interface IEntity
    {
        int ID { get; }
    }
}
