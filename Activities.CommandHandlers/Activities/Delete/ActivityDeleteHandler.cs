﻿using Activities.CommandHandlers.Base;
using Activities.Domain.Activities;
using Activities.Models.Activities;

namespace Activities.CommandHandlers.Activities.Delete
{
    public class ActivityDeleteHandler : DeleteHandler<ActivityModel, Activity>
    {
        public ActivityDeleteHandler(IActivityRepository repository) : base(repository)
        { }
    }
}
