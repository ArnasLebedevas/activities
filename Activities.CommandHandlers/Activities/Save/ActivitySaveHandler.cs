﻿using Activities.CommandHandlers.Base;
using Activities.Domain.Activities;
using Activities.Models.Activities;
using Activities.Models.Base.Commands;

namespace Activities.CommandHandlers.Activities.Save
{
    public class ActivitySaveHandler : SaveHandler<ActivityModel, Activity>
    {
        public ActivitySaveHandler(IActivityRepository repository, IMapper<ActivityModel, Activity> mapper)
            : base(repository, mapper)
        { }
    }
}
