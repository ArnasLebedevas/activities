﻿using Activities.Domain.Activities;
using Activities.Models.Activities;
using Activities.Models.Base.Commands;

namespace Activities.CommandHandlers.Activities.Save
{
    public class ActivityMapper : IMapper<ActivityModel, Activity>
    {
        public void Map(ActivityModel model, Activity entity)
        {
            entity.ID = model.ID;
            entity.Title = model.Title;
            entity.Date = model.Date;
            entity.Description = model.Description;
            entity.Category = model.Category;
            entity.City = model.City;
            entity.Venue = model.Venue;
        }
    }
}
