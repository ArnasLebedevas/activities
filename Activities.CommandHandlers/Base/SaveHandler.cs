﻿using Activities.Domain;
using Activities.Models;
using Activities.Models.Base;
using Activities.Models.Base.Commands;
using System.Threading.Tasks;

namespace Activities.CommandHandlers.Base
{
    public abstract class SaveHandler<TModel, TEntity> : ICommandHandler<TModel>
        where TModel : IEntityModel
        where TEntity : IEntity, new()
    {
        protected IRepository<TEntity> repository;
        protected IMapper<TModel, TEntity> mapper;

        public SaveHandler(IRepository<TEntity> repository, IMapper<TModel, TEntity> mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public virtual async Task<ICommandResult> Handle(TModel model)
        {
            var entity = await MapEntity(model);

            if(model.ID == 0)
                repository.Add(entity);

            await repository.SaveAsync();

            return new SuccessResult();
        }

        private async Task<TEntity> MapEntity(TModel model)
        {
            TEntity entity;

            if (model.ID == 0)
                entity = new TEntity();
            else
                entity = await repository.GetAsync(model.ID);

            mapper.Map(model, entity);

            return entity;
        }
    }
}
