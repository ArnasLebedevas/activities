﻿using Activities.Domain;
using Activities.Models;
using Activities.Models.Base;
using Activities.Models.Base.Commands;
using System.Threading.Tasks;

namespace Activities.CommandHandlers.Base
{
    public abstract class DeleteHandler<TModel, TEntity> : ICommandHandler<DeleteCommand<TModel>>
        where TEntity : IEntity
        where TModel : IEntityModel
    {
        private readonly IRepository<TEntity> repository;

        public DeleteHandler(IRepository<TEntity> repository)
        {
            this.repository = repository;
        }

        public virtual async Task<ICommandResult> Handle(DeleteCommand<TModel> model)
        {
            var entity = await repository.GetAsync(model.ID);

            repository.Remove(entity);

            await repository.SaveAsync();

            return new CommandResult();
        }
    }
}
