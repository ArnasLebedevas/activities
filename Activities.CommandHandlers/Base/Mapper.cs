﻿using Activities.Models.Base.Commands;
using AutoMapper;

namespace Activities.CommandHandlers.Base
{
    public class Mapper<TModel, TEntity> : IMapper<TModel, TEntity>
    {
        public void Map(TModel model, TEntity entity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TModel, TEntity>();
            });

            var mapper = config.CreateMapper();

            mapper.Map(model, entity);
        }
    }
}
