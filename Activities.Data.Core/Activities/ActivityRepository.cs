﻿using Activities.Data.Core.Data;
using Activities.Domain.Activities;
using Activities.Models.Base.Infrastructure;

namespace Activities.Data.Core.Activities
{
    public class ActivityRepository : DataRepository<Activity>, IActivityRepository
    {
        public ActivityRepository(IWriteContext context) : base(context)
        { }
    }
}
