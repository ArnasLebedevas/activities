﻿using Activities.Domain;
using Activities.Models.Base.Infrastructure;

namespace Activities.Data.Core.Data
{
    public class DataRepository<TEntity> : Repository<TEntity>
        where TEntity : class, IEntity
    {
        public DataRepository(IWriteContext context) : base(context)
        { }
    }
}
