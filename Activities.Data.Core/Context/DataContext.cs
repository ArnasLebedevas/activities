﻿using Activities.Domain.Activities;
using Microsoft.EntityFrameworkCore;

namespace Activities.Data.Core.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        { }

        public DbSet<Activity> Activities { get; set; }
    }
}
