﻿using System.Threading.Tasks;

namespace Activities.Models.Base
{
    public interface IBus
    {
        Task<ICommandResult> Send<TCommand>(TCommand command);
        Task<TResult> Query<TParam, TResult>(TParam param);
    }
}
