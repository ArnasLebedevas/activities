﻿using System.Threading.Tasks;

namespace Activities.Models.Base
{
    public interface ICommandHandler<TCommand>
    {
        Task<ICommandResult> Handle(TCommand command);
    }
}
