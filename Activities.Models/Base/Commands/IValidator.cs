﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Activities.Models.Base.Commands
{
    public interface IValidator
    {
        Task<ICommandResult> Validate<TCommand>(TCommand command, IEnumerable<IRule<TCommand>> rules);
    }
}
