﻿namespace Activities.Models.Base.Commands
{
    public class FailureResult : CommandResult
    {
        public FailureResult()
        { }

        public FailureResult(string message)
        {
            AddError(string.Empty, message);
        }
    }
}
