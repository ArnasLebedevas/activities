﻿using System.Threading.Tasks;

namespace Activities.Models.Base.Commands
{
    public interface IRule<TCommand>
    {
        Task<ICommandResult> Validate(TCommand command);
    }
}
