﻿namespace Activities.Models.Base.Commands
{
    public interface IMapper<TModel, TEntity>
    {
        void Map(TModel model, TEntity entity);
    }
}
