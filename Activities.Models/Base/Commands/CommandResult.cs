﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Activities.Models.Base.Commands
{
    public class CommandResult : ReadOnlyDictionary<string, IList<string>>, ICommandResult
    {
        public CommandResult() : base(new Dictionary<string, IList<string>>())
        { }

        public bool IsValid
        {
            get { return isValid; }
            private set { isValid = value; }
        }

        private bool isValid = true;

        public void AddError(string key, string message)
        {
            isValid = false;

            if (Dictionary.ContainsKey(key))
            {
                var messages = Dictionary[key];

                var exists = messages.Any(o => o == message);

                if (!exists)
                    messages.Add(message);
            }
            else
            {
                Dictionary.Add(key, new List<string> { message });
            }
        }

        public void Merge(ICommandResult result)
        {
            if (result == null)
                return;

            foreach (var entry in result)
            {
                foreach (var message in entry.Value)
                {
                    AddError(entry.Key, message);
                }
            }
        }
    }
}
