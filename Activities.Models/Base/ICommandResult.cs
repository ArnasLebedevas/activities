﻿using System.Collections.Generic;

namespace Activities.Models.Base
{
    public interface ICommandResult : IReadOnlyDictionary<string, IList<string>>
    {
        bool IsValid { get; }

        void AddError(string key, string message);
        void Merge(ICommandResult result);
    }
}
