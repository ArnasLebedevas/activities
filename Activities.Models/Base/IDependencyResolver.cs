﻿namespace Activities.Models.Base
{
    public interface IDependencyResolver
    {
        T Resolve<T>();
    }
}
