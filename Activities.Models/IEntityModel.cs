﻿namespace Activities.Models
{
    public interface IEntityModel
    {
        int ID { get; set; }
    }
}
